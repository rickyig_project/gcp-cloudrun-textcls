<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/rickyig_project/ml-modeling-text-classification">
    <img src="assets/img/logo.png" alt="Logo" width="150" height="150">
  </a>

<h3 align="center">Text Document Classification</h3>

  <p align="center">
    Handles text classification using the BBC dataset which consists of thousands of text files that have been segregated into different classes.
    <br />
    <a href="https://gitlab.com/rickyig_project/ml-modeling-text-classification"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/rickyig_project/ml-modeling-text-classification">View Demo</a>
    ·
    <a href="https://gitlab.com/rickyig_project/ml-modeling-text-classification/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/rickyig_project/ml-modeling-text-classification/issues">Request Feature</a>
    ·
    <a href="https://text-classification-app-gjipdyrvm7qxwavdhkuchd.streamlit.app/">Web Demo</a>
  </p>
</div>

# Data Processing Pipeline for AI Projects

Welcome to our AI project's repository! This README provides a detailed guide on our data processing pipeline, ensuring clarity and reproducibility in our workflows. Here, you will find step-by-step instructions on how we handle data cleaning, feature extraction, feature engineering, data splitting, versioning, modeling, MLOps - Neptune.ai, and model serving.

## Table of Contents
- [Project Overview](#project-overview)
- [Data Processing Pipeline](#data-processing-pipeline)
  - [Data Cleaning](#data-cleaning)
  - [Feature Extraction](#feature-extraction)
  - [Feature Engineering](#feature-engineering)
  - [Data Splitting](#data-splitting)
  - [Data Versioning](#data-versioning)
  - [Modeling](#modeling)
  - [MLOps - Neptune.ai](#mlops---neptuneai)
  - [Model Serving](#model-serving)
- [Getting Started](#getting-started)
- [Contributing](#contributing)
- [License](#license)

## Project Overview
Dalam domain analisis teks, klasifikasi dokumen merupakan tantangan yang sering dihadapi, khususnya dalam mengelola volume data yang besar. Proyek ini menangani klasifikasi teks menggunakan dataset BBC yang terdiri dari ribuan file teks yang telah tersegresi ke dalam kelas-kelas yang berbeda. Masing-masing kelas mewakili kategori berita tertentu. Klasifikasi yang akurat dan efisien dari dokumen-dokumen ini menjadi penting untuk pengelolaan informasi yang lebih baik dan akses yang cepat terhadap konten yang relevan. Klasifikasi ini harus mengatasi berbagai tantangan seperti pengelolaan teks dalam jumlah besar, pengurangan dimensi, dan pengambilan fitur yang efektif untuk meningkatkan performa prediksi.

## Goals
- Pengembangan Model Klasifikasi: Mengembangkan model machine learning yang dapat secara akurat mengklasifikasikan teks-teks berita ke dalam kategori yang relevan seperti politik, bisnis, olahraga, dll.
- Optimalisasi dan Evaluasi Model: Melakukan tuning parameter pada model yang dipilih untuk meningkatkan akurasi klasifikasi. Model harus dievaluasi menggunakan metrik seperti akurasi, presisi, recall, dan F1-score.
- Automasi Preprocessing Data: Mengembangkan pipeline preprocessing yang dapat secara otomatis melakukan tokenisasi, stopword removal, dan vektorisasi teks.
- Interpretasi Hasil: Memberikan wawasan tentang pengelompokan kelas dan menentukan fitur-fitur kunci yang paling mempengaruhi prediksi klasifikasi.
- Deployment Model: Menyiapkan model yang telah dikembangkan agar dapat digunakan dalam aplikasi produksi yang membutuhkan klasifikasi teks secara real-time.
Dengan mencapai tujuan ini, proyek akan memberikan solusi yang robust untuk klasifikasi teks berbasis kategori berita, mempercepat proses pengambilan informasi yang relevan dan meningkatkan manajemen konten berbasis teks.

## Data Processing Pipeline

### Data Cleaning
**Objective:** Ensure that the data is free of errors or inconsistencies, and is ready for analysis and modeling.

**Steps:**
1. **Identify the .txt document dataset and its contents.**
2. **Preprocess Data using Regex**

**Tools & Libraries:**
- Pandas for data manipulation
- NumPy for numerical operations
- Scikit-learn for preprocessing

### Feature Extraction
**Objective:** Transform raw data into meaningful features suitable for model building.

**Steps:**
1. **Text Data:** Use NLP techniques to extract features from text such as bag of words, TF-IDF, etc. In this case, we're using TF-IDF.

**Tools & Libraries:**
- NLTK/SpaCy for natural language processing

### Feature Engineering
**Objective:** Enhance the predictive power of the input data by creating new features or modifying existing ones.

**Steps:**
1. **WordNetLemmatizer**

**Tools & Libraries:**
- Scikit-learn for creating and selecting features

### Data Splitting
**Objective:** Divide the data into training, validation, and test sets to ensure robust model evaluation.

**Steps:**
1. **Train & Test Split:** : Train Test split -> 0.25

**Tools & Libraries:**
- Scikit-learn for splitting data

### Data Versioning
**Objective:** Keep track of different versions of datasets used in the project.

**Steps:**
1. **Version Control:** Use tools like GitLab/DVC to manage and version control datasets.
2. **Documentation:** Document the changes in each version, including the rationale for changes and impact on models.

**Tools & Libraries:**
- GitLab

### Modeling
**Objective:** Create a machine learning model. Model used: Logistic Regression, Random Forest, SVC

**Tools & Libraries:**
- Scikit-learn

### MLOps - Neptune.ai
**Objective:** Neptune is a metadata store for MLOps, built for research and production teams that run a lot of experiments. It gives you a central place to log, store, display, organize, compare, and query all metadata generated during the machine learning lifecycle.

**Tools & Libraries:**
- Neptune.ai

## Getting Started
Provide instructions on how to set up and run the project locally. This section should include:
- Installation of required libraries
- How to run the scripts
- Example commands


```bash
# Clone the repository
git clone https://gitlab.com/rickyig_project/bbc-text-classification-rickyig.git
cd bbc-text-classification-rickyig

# Run the train.py and see/monitor Neptune.ai
```

## Experimented on Neptune.ai

Contohnya ketika menggunakan <b>SVM (SVC)</b>
<div align="center">
  <img src="assets/img/TEX8.png" alt="Tex8" width="1920" height="1080">
</div>

<br>
Contohnya ketika menggunakan <b>Random Forest</b>
<div align="center">
  <img src="assets/img/TEX9.png" alt="Tex9" width="1920" height="1080">
</div>

<br>
Contohnya ketika menggunakan <b>Logistic Regression</b>
<div align="center">
  <img src="assets/img/TEX10.png" alt="Tex10" width="1920" height="1080">
</div>

## Model Serving
- Model Version: v1.0.0
- Data Version: v1.0.0
- Vectornizer Version: v1.0.0
- Run_ID: TEX-21

- Data Management System Link: https://gitlab.com/rickyig_project/text-classification-data-management-system
- Model Management System Link: https://gitlab.com/rickyig_project/ml-modeling-text-classification
- Docker Image Repository : https://hub.docker.com/repository/docker/rickyindrag/rickyindrag_text_cls/general

### How to set up :

Via docker:

`docker image pull rickyindrag/rickyindrag_text_cls:latest`

Via local:

`git clone https://gitlab.com/rickyig_project/ml-model-serving-text-classification`

`pip install requirements.txt`


### How to run :

**Docker**

```docker run --name rickyindrag_text_cls -e UNCERTAINTY_THRESHOLD=0.5 -e HOST=0.0.0.0 -e PORT=8080 -p 8080:8080 -d rickyindrag_text_cls:latest```


**Local**

`python app.py`

### Include:

- [x] Nilai + jika model management nya pakai neptune [Done]
- [x] Nilai + preprocessor/vectorizer pakai neptune [Done]

### Contoh Demo:
<div align="center">
  <img src="assets/img/cls7.png" alt="cls7" width="1920" height="1080">
</div>
<div align="center">
  <img src="assets/img/cls8.png" alt="cls8" width="1920" height="1080">
</div>
<div align="center">
  <img src="assets/img/cls9.png" alt="cls9" width="1920" height="1080">
</div>
<div align="center">
  <img src="assets/img/cls10.png" alt="cls10" width="1920" height="1080">
</div>

## Contributing
Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License
Made by Ricky Indra Gunawan